/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmsControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//alarms.cql", true, true, "worldview_dev1"));

    static AlarmsController instance;
    static String rowId;

    public AlarmsControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new AlarmsController();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createAlarm method, of class AlarmsController. Happy path: Passing input correctly and achieving desired output.
     */
    @Test
    public void test11_CreateAlarm() {
        System.out.println("test11_CreateAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"New Alarm created successfully.\"}";
        String result = instance.createAlarm(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createAlarm method, of class AlarmsController. Unhappy path: Missing required field 'paramName' in input json.
     */
    @Test
    public void test12_CreateAlarm() {
        System.out.println("test12_CreateAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createAlarm(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createAlarm method, of class AlarmsController. Unhappy path: Missing input json.
     */
    @Test
    public void test13_CreateAlarm() {
        System.out.println("test13_CreateAlarm");
        String content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createAlarm(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createAlarm method, of class AlarmsController. Unhappy path: Passing invalid pointId in input json.
     */
    @Test
    public void test14_CreateAlarm() {
        System.out.println("test14_CreateAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"615611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Error: Failed to find point.\"}";
        String result = instance.createAlarm(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteArea method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test21_GetAlarmsByCustomerSiteArea() throws Exception {
        System.out.println("test21_GetAlarmsByCustomerSiteArea");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":false,\"deleted\":false,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteArea(customer, siteId, areaId);
        System.out.println("test2_GetAlarmsByCustomerSiteArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_GetAlarmsByCustomerSiteArea() throws Exception {
        System.out.println("test22_GetAlarmsByCustomerSiteArea");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        fail(instance.getAlarmsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getAlarmsByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test23_GetAlarmsByCustomerSiteArea() throws Exception {
        System.out.println("test23_GetAlarmsByCustomerSiteArea");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        fail(instance.getAlarmsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getAlarmsByCustomerSiteArea method, of class AlarmsController. Unhappy path: When the customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test24_GetAlarmsByCustomerSiteArea() throws Exception {
        System.out.println("test24_GetAlarmsByCustomerSiteArea");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteArea(customer, siteId, areaId);
        System.out.println("test2_GetAlarmsByCustomerSiteArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test25_GetAlarmsByCustomerSiteArea() throws Exception {
        System.out.println("test25_GetAlarmsByCustomerSiteArea");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        fail(instance.getAlarmsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAsset method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test31_GetAlarmsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test31_GetAlarmsByCustomerSiteAreaAsset");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":false,\"deleted\":false,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        System.out.println("test3_GetAlarmsByCustomerSiteAreaAsset result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetAlarmsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test32_GetAlarmsByCustomerSiteAreaAsset");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        fail(instance.getAlarmsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_GetAlarmsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test33_GetAlarmsByCustomerSiteAreaAsset");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        fail(instance.getAlarmsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test34_GetAlarmsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test34_GetAlarmsByCustomerSiteAreaAsset");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        System.out.println("test3_GetAlarmsByCustomerSiteAreaAsset result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test35_GetAlarmsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test35_GetAlarmsByCustomerSiteAreaAsset");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        fail(instance.getAlarmsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test41_GetAlarmsByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test41_GetAlarmsByCustomerSiteAreaAssetPtLoc");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":false,\"deleted\":false,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId);
        System.out.println("test4_GetAlarmsByCustomerSiteAreaAssetPtLoc result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test42_GetAlarmsByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test42_GetAlarmsByCustomerSiteAreaAssetPtLoc");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId);
        System.out.println("test4_GetAlarmsByCustomerSiteAreaAssetPtLoc result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test43_GetAlarmsByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test43_GetAlarmsByCustomerSiteAreaAssetPtLoc");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test44_GetAlarmsByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test44_GetAlarmsByCustomerSiteAreaAssetPtLoc");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test45_GetAlarmsByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test45_GetAlarmsByCustomerSiteAreaAssetPtLoc");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test51_GetAlarmsByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test51_GetAlarmsByCustomerSiteAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":false,\"deleted\":false,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId);
        System.out.println("test5_GetAlarmsByCustomerSiteAreaAssetPtLocPoint result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test52_GetAlarmsByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test52_GetAlarmsByCustomerSiteAreaAssetPtLocPoint");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId);
        System.out.println("test5_GetAlarmsByCustomerSiteAreaAssetPtLocPoint result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test53_GetAlarmsByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test53_GetAlarmsByCustomerSiteAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test54_GetAlarmsByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test54_GetAlarmsByCustomerSiteAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test55_GetAlarmsByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test54_GetAlarmsByCustomerSiteAreaAssetPtLocPoint");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPointParam method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test511_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test511_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":false,\"deleted\":false,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAssetPtLocPointParam(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName);
        System.out.println("test511_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test512_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test512_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteAreaAssetPtLocPointParam(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName);
        System.out.println("test512_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test513_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test513_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLocPointParam(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test514_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test514_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLocPointParam(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName));
    }

    /**
     * Test of getAlarmsByCustomerSiteAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test515_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test515_GetAlarmsByCustomerSiteAreaAssetPtLocPointParam");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        fail(instance.getAlarmsByCustomerSiteAreaAssetPtLocPointParam(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName));
    }

    /**
     * Test of getAlarmsByPK method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test61_GetAlarmsByPK() throws Exception {
        System.out.println("test61_GetAlarmsByPK");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":false,\"deleted\":false,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType);
        System.out.println("test61_GetAlarmsByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByPK method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test62_GetAlarmsByPK() throws Exception {
        System.out.println("test62_GetAlarmsByPK");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType);
        System.out.println("test6_GetAlarmsByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByPK method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test63_GetAlarmsByPK() throws Exception {
        System.out.println("test63_getAlarmsByPK");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType));
    }

    /**
     * Test of getAlarmsByPK method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test64_GetAlarmsByPK() throws Exception {
        System.out.println("test64_getAlarmsByPK");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType));
    }

    /**
     * Test of getAlarmsByPK method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test65_GetAlarmsByPK() throws Exception {
        System.out.println("test65_GetAlarmsByPK");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLoc method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test71_GetAlarmsByCustomerSiteYearAreaAssetPtLoc() throws Exception {
        System.out.println("test71_getAlarmsByCustomerSiteYearAreaAssetPtLoc");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        short createdYear = 2022;
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer, siteId, createdYear, areaId, assetId, pointLocationId);
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        System.out.println("test7_GetAlarmsByCustomerSiteYearAreaAssetPtLoc result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test72_GetAlarmsByCustomerSiteYearAreaAssetPtLoc() throws Exception {
        System.out.println("test72_GetAlarmsByCustomerSiteYearAreaAssetPtLoc");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        short createdYear = 2022;
        instance.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer, siteId, createdYear, areaId, assetId, pointLocationId);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test73_GetAlarmsByCustomerSiteYearAreaAssetPtLoc() throws Exception {
        System.out.println("test73_GetAlarmsByCustomerSiteYearAreaAssetPtLoc");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        short createdYear = 2022;
        instance.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer, siteId, createdYear, areaId, assetId, pointLocationId);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test74_GetAlarmsByCustomerSiteYearAreaAssetPtLoc() throws Exception {
        System.out.println("test74_GetAlarmsByCustomerSiteYearAreaAssetPtLoc");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        short createdYear = 2022;
        instance.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer, siteId, createdYear, areaId, assetId, pointLocationId);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test82_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint() throws Exception {
        System.out.println("test82_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId);
        System.out.println("test8_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test83_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint() throws Exception {
        System.out.println("test83_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = "null";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test84_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint() throws Exception {
        System.out.println("test84_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = null;
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test85_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint() throws Exception {
        System.out.println("test85_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test902_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test902_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName);
        System.out.println("test90_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test903_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test903_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam");
        String customer = "77777";
        String siteId = "null";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test904_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test904_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam");
        String customer = "77777";
        String siteId = null;
        short createdYear = 2022;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test905_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test905_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2022;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType method of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test9002_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType() throws Exception {
        System.out.println("test9002_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType);
        System.out.println("test9001_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType method of class AlarmsController. Unhappy path: When siteId is passed as "null", no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test9003_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType() throws Exception {
        System.out.println("test9003_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType");
        String customer = "77777";
        String siteId = "null";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType method of class AlarmsController. Unhappy path: When siteId is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test9004_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType() throws Exception {
        System.out.println("test9004_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType");
        String customer = "77777";
        String siteId = null;
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType method of class AlarmsController. Unhappy path: When siteId is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test9005_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType() throws Exception {
        System.out.println("test9005_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2023;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test912_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow() throws Exception {
        System.out.println("test912_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2022;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, "60685b3e-758a-4295-a243-3d600d8d75b8");
        System.out.println("test91_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test913_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow() throws Exception {
        System.out.println("test913_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow");
        String customer = "77777";
        String siteId = "null";
        short createdYear = 2022;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, rowId));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test914_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow() throws Exception {
        System.out.println("test914_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow");
        String customer = "77777";
        String siteId = null;
        short createdYear = 2022;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, rowId));
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test915_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow() throws Exception {
        System.out.println("test915_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2022;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        fail(instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, rowId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test921_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test921_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"alarmCount\":1,\"alertCount\":1,\"faultCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmCountByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId);
        System.out.println("test92_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test922_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test922_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmCountByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId);
        System.out.println("test92_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test923_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test923_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmCountByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test924_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test924_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmCountByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLocPoint method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test925_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint() throws Exception {
        System.out.println("test925_GetAlarmCountByCustomerSiteAreaAssetPtLocPoint");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        fail(instance.getAlarmCountByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test931_GetAlarmCountByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test931_GetAlarmCountByCustomerSiteAreaAssetPtLoc");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"alarmCount\":1,\"alertCount\":1,\"faultCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmCountByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId);
        System.out.println("test93_GetAlarmCountByCustomerSiteAreaAssetPtLoc result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test932_GetAlarmCountByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test932_GetAlarmCountByCustomerSiteAreaAssetPtLoc");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmCountByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId);
        System.out.println("test93_GetAlarmCountByCustomerSiteAreaAssetPtLoc result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test933_GetAlarmCountByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test933_GetAlarmCountByCustomerSiteAreaAssetPtLoc");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        fail(instance.getAlarmCountByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test934_GetAlarmCountByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test934_GetAlarmCountByCustomerSiteAreaAssetPtLoc");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        fail(instance.getAlarmCountByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAssetPtLoc method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test935_GetAlarmCountByCustomerSiteAreaAssetPtLoc() throws Exception {
        System.out.println("test935_GetAlarmCountByCustomerSiteAreaAssetPtLoc");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        fail(instance.getAlarmCountByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAsset method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test941_GetAlarmCountByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test941_GetAlarmCountByCustomerSiteAreaAsset");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"alarmCount\":1,\"alertCount\":1,\"faultCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmCountByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        System.out.println("test94_GetAlarmCountByCustomerSiteAreaAsset result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test942_GetAlarmCountByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test942_GetAlarmCountByCustomerSiteAreaAsset");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmCountByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        System.out.println("test94_GetAlarmCountByCustomerSiteAreaAsset result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test943_GetAlarmCountByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test943_GetAlarmCountByCustomerSiteAreaAsset");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        fail(instance.getAlarmCountByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test944_GetAlarmCountByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test944_GetAlarmCountByCustomerSiteAreaAsset");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        fail(instance.getAlarmCountByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteAreaAsset method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test945_GetAlarmCountByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test945_GetAlarmCountByCustomerSiteAreaAsset");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        fail(instance.getAlarmCountByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test951_GetAlarmCountByCustomerSiteArea() throws Exception {
        System.out.println("test951_GetAlarmCountByCustomerSiteArea");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"alarmCount\":1,\"alertCount\":1,\"faultCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmCountByCustomerSiteArea(customer, siteId, areaId);
        System.out.println("test95_GetAlarmCountByCustomerSiteArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test952_GetAlarmCountByCustomerSiteArea() throws Exception {
        System.out.println("test952_GetAlarmCountByCustomerSiteArea");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmCountByCustomerSiteArea(customer, siteId, areaId);
        System.out.println("test95_GetAlarmCountByCustomerSiteArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test953_GetAlarmCountByCustomerSiteArea() throws Exception {
        System.out.println("test953_GetAlarmCountByCustomerSiteArea");
        String customer = "77777";
        String siteId = "null";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        fail(instance.getAlarmCountByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test954_GetAlarmCountByCustomerSiteArea() throws Exception {
        System.out.println("test954_GetAlarmCountByCustomerSiteArea");
        String customer = "77777";
        String siteId = null;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        fail(instance.getAlarmCountByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test955_GetAlarmCountByCustomerSiteArea() throws Exception {
        System.out.println("test955_GetAlarmCountByCustomerSiteArea");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        fail(instance.getAlarmCountByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test961_GetAlarmCountByCustomerSite() throws Exception {
        System.out.println("test961_GetAlarmCountByCustomerSite");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"alarmCount\":1,\"alertCount\":1,\"faultCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmCountByCustomerSite(customer, siteId);
        System.out.println("test95_GetAlarmCountByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: When customer is passed as null, no data is received from the database.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test962_GetAlarmCountByCustomerSite() throws Exception {
        System.out.println("test962_GetAlarmCountByCustomerSite");
        String customer = "null";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAlarmCountByCustomerSite(customer, siteId);
        System.out.println("test95_GetAlarmCountByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null".
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test963_GetAlarmCountByCustomerSite() throws Exception {
        System.out.println("test963_GetAlarmCountByCustomerSite");
        String customer = "77777";
        String siteId = "null";
        fail(instance.getAlarmCountByCustomerSite(customer, siteId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when siteId is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test964_GetAlarmCountByCustomerSite() throws Exception {
        System.out.println("test964_GetAlarmCountByCustomerSite");
        String customer = "77777";
        String siteId = null;
        fail(instance.getAlarmCountByCustomerSite(customer, siteId));
    }

    /**
     * Test of getAlarmCountByCustomerSiteArea method, of class AlarmsController. Unhappy path: Controller throws NullPointerException when customer is passed as null.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test965_GetAlarmCountByCustomerSite() throws Exception {
        System.out.println("test965_GetAlarmCountByCustomerSite");
        String customer = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        fail(instance.getAlarmCountByCustomerSite(customer, siteId));
    }

    /**
     * Test of updateAlarm method, of class AlarmsController. Happy path: Passing input correctly and achieving desired output.
     */
    @Test
    public void test971_UpdateAlarm() {
        System.out.println("test971_UpdateAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                //                + "    \"rowId\" : \"" + rowId + "\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : true,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Updated Alarms successfully.\"}";
        String result = instance.updateAlarm(content);
        System.out.println("json - " + content);
        System.out.println("test971_UpdateAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLoc method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test972_GetAlarmsByCustomerSiteYearAreaAssetPtLoc() throws Exception {
        System.out.println("test972_GetAlarmsByCustomerSiteYearAreaAssetPtLoc");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        short createdYear = 2021;
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer, siteId, createdYear, areaId, assetId, pointLocationId);
        rowId = result.substring(result.indexOf("\"rowId\":") + 9, result.indexOf("\"rowId\":") + 45);
        System.out.println(" rowId=" + rowId);
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":\"2021\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":2021,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"rowId\":\"" + rowId + "\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":true,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("test972_GetAlarmsByCustomerSiteYearAreaAssetPtLoc result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPoint method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test972_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint() throws Exception {
        System.out.println("test81_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2021;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":\"2021\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":2021,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"rowId\":\"" + rowId + "\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":true,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId);
        System.out.println("test8_GetAlarmsByCustomerSiteYearAreaAssetPtLocPoint result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test972_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam() throws Exception {
        System.out.println("test901_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2021;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":\"2021\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":2021,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"rowId\":\"" + rowId + "\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":true,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName);
        System.out.println("test90_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParam result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType method of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test972_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType() throws Exception {
        System.out.println("test9001_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2021;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":\"2021\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":2021,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"rowId\":\"" + rowId + "\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":true,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType);
        System.out.println("test9001_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamRow method, of class AlarmsController. Happy path: Passing all inputs correctly and achieving desired output.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test972_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow() throws Exception {
        System.out.println("test911_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow");
        String customer = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        short createdYear = 2021;
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String pointLocationId = "665611d8-382f-4c95-b5af-a23432e59dac";
        String pointId = "60685b3e-758a-4295-a243-3d600d8d75b5";
        String paramName = "Peak2Peak";
        String alarmType = "HIGHALERT";
        String expResult = "{\"customer\":\"77777\",\"createdYear\":\"2021\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"rowId\":\"" + rowId + "\",\"Alarms\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"createdYear\":2021,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"paramName\":\"Peak2Peak\",\"alarmType\":\"HIGHALERT\",\"rowId\":\"" + rowId + "\",\"exceedPercent\":10.0,\"highAlert\":20.0,\"highFault\":30.0,\"acknowledged\":true,\"lowAlert\":10.0,\"lowFault\":5.0,\"paramType\":\"Test Type 1\",\"sampleTimestamp\":\"2021-01-01T01:00:00+0000\",\"sensorType\":\"TEMP\",\"trendValue\":22.0,\"triggerTime\":\"2021-01-02T01:00:00+0000\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(customer, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, rowId);
        System.out.println("test911_GetAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateAlarm method, of class AlarmsController. Unhappy path: Missing required field 'pointId' in input json.
     */
    @Test
    public void test972_UpdateAlarm() {
        System.out.println("test972_UpdateAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"rowId\" : \"" + rowId + "\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateAlarm(content);
        System.out.println("test972_UpdateAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateAlarm method, of class AlarmsController. Unhappy path: Passing empty json.
     */
    @Test
    public void test973_UpdateAlarm() {
        System.out.println("test973_UpdateAlarm");
        String content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateAlarm(content);
        System.out.println("test973_UpdateAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateAlarm method, of class AlarmsController. Unhappy path: Passing invalid 'pointId' in input json.
     */
    @Test
    public void test974_UpdateAlarm() {
        System.out.println("test974_UpdateAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60385b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"rowId\" : \"" + rowId + "\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Alarms not found to update.\"}";
        String result = instance.updateAlarm(content);
        System.out.println("test974_UpdateAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAlarm method, of class AlarmsController. Happy path: Passing input correctly and achieving desired output.
     */
    //@Test
    //Delete not needed since activeAlarms is being deleted in update method when an alarm is acknowledged
    public void test981_DeleteAlarm() {
        System.out.println("test981_DeleteAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"rowId\" : \"" + rowId + "\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : true,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        System.out.println("test981_DeleteAlarm json = " + content);
        String expResult = "{\"outcome\":\"Deleted Alarms successfully.\"}";
        String result = instance.deleteAlarm(content);
        System.out.println("test981_DeleteAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAlarm method, of class AlarmsController. Unhappy path: Missing required field 'pointId' in input json.
     */
    @Test
    public void test982_DeleteAlarm() {
        System.out.println("test982_DeleteAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"rowId\" : \"" + rowId + "\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Error: Failed to delete Alarms.\"}";
        String result = instance.deleteAlarm(content);
        System.out.println("test982_DeleteAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAlarm method, of class AlarmsController. Unhappy path: Passing invalid/empty input json.
     */
    @Test
    public void test983_DeleteAlarm() {
        System.out.println("test983_DeleteAlarm");
        String content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteAlarm(content);
        System.out.println("test983_DeleteAlarm result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAlarm method, of class AlarmsController. Unhappy path: Passing invalid 'pointId' in input json.
     */
    @Test
    public void test984_DeleteAlarm() {
        System.out.println("test984_DeleteAlarm");
        String content = "{\n"
                + "    \"customerAccount\" : \"77777\",\n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60585b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"rowId\" : \"" + rowId + "\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\",\n"
                + "    \"alarmType\" : \"HIGHALERT\",\n"
                + "    \"exceedPercent\" : 10,\n"
                + "    \"highAlert\" : 20,\n"
                + "    \"highFault\" : 30,\n"
                + "    \"lowAlert\" : 10,\n"
                + "    \"lowFault\" : 5,\n"
                + "    \"acknowledged\" : false,\n"
                + "    \"deleted\" : false,\n"
                + "    \"paramType\" : \"Test Type 1\",\n"
                + "    \"sampleTimestamp\" : \"2021-01-01T01:00:00Z\",\n"
                + "    \"sensorType\" : \"TEMP\",\n"
                + "    \"trendValue\" : 22,\n"
                + "    \"triggerTime\" : \"2021-01-02T01:00:00Z\"\n"
                + "}";
        String expResult = "{\"outcome\":\"No Alarms found to delete.\"}";
        String result = instance.deleteAlarm(content);
        System.out.println("test984_DeleteAlarm result - " + result);
        assertEquals(expResult, result);
    }

}
