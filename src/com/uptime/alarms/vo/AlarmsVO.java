/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.vo;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class AlarmsVO {

    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private String paramName;
    private String alarmType;
    private UUID apSetId;
    private int createdDate;
    private float exceedPercent;
    private float highAlert;
    private float highFault;
    private boolean acknowledged;
    private boolean deleted;
    private float lowAlert;
    private float lowFault;
    private String paramType;
    private Instant sampleTimestamp;
    private String sensorType;
    private float trendValue;
    private Instant triggerTime;
    private UUID waveId;
    private UUID rowId;
    private short createdYear;
    private int alarmCount;
    private boolean trigger;
    private int alertCount;
    private int faultCount;

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(int createdDate) {
        this.createdDate = createdDate;
    }

    public float getExceedPercent() {
        return exceedPercent;
    }

    public void setExceedPercent(float exceedPercent) {
        this.exceedPercent = exceedPercent;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public Instant getSampleTimestamp() {
        return sampleTimestamp;
    }

    public void setSampleTimestamp(Instant sampleTimestamp) {
        this.sampleTimestamp = sampleTimestamp;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public float getTrendValue() {
        return trendValue;
    }

    public void setTrendValue(float trendValue) {
        this.trendValue = trendValue;
    }

    public Instant getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(Instant triggerTime) {
        this.triggerTime = triggerTime;
    }

    public UUID getWaveId() {
        return waveId;
    }

    public void setWaveId(UUID waveId) {
        this.waveId = waveId;
    }

    public UUID getRowId() {
        return rowId;
    }

    public void setRowId(UUID rowId) {
        this.rowId = rowId;
    }

    public short getCreatedYear() {
        return createdYear;
    }

    public void setCreatedYear(short createdYear) {
        this.createdYear = createdYear;
    }

    public int getAlarmCount() {
        return alarmCount;
    }

    public void setAlarmCount(int alarmCount) {
        this.alarmCount = alarmCount;
    }

    public boolean isTrigger() {
        return trigger;
    }

    public void setTrigger(boolean trigger) {
        this.trigger = trigger;
    }

    public int getAlertCount() {
        return alertCount;
    }

    public void setAlertCount(int alertCount) {
        this.alertCount = alertCount;
    }

    public int getFaultCount() {
        return faultCount;
    }

    public void setFaultCount(int faultCount) {
        this.faultCount = faultCount;
    }

    
    
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.customerAccount);
        hash = 83 * hash + Objects.hashCode(this.siteId);
        hash = 83 * hash + Objects.hashCode(this.areaId);
        hash = 83 * hash + Objects.hashCode(this.assetId);
        hash = 83 * hash + Objects.hashCode(this.pointLocationId);
        hash = 83 * hash + Objects.hashCode(this.pointId);
        hash = 83 * hash + Objects.hashCode(this.paramName);
        hash = 83 * hash + Objects.hashCode(this.alarmType);
        hash = 83 * hash + Objects.hashCode(this.apSetId);
        hash = 83 * hash + this.createdDate;
        hash = 83 * hash + Float.floatToIntBits(this.exceedPercent);
        hash = 83 * hash + Float.floatToIntBits(this.highAlert);
        hash = 83 * hash + Float.floatToIntBits(this.highFault);
        hash = 83 * hash + (this.acknowledged ? 1 : 0);
        hash = 83 * hash + (this.deleted ? 1 : 0);
        hash = 83 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 83 * hash + Float.floatToIntBits(this.lowFault);
        hash = 83 * hash + Objects.hashCode(this.paramType);
        hash = 83 * hash + Objects.hashCode(this.sampleTimestamp);
        hash = 83 * hash + Objects.hashCode(this.sensorType);
        hash = 83 * hash + Float.floatToIntBits(this.trendValue);
        hash = 83 * hash + Objects.hashCode(this.triggerTime);
        hash = 83 * hash + Objects.hashCode(this.waveId);
        hash = 83 * hash + Objects.hashCode(this.rowId);
        hash = 83 * hash + this.createdYear;
        hash = 83 * hash + this.alarmCount;
        hash = 83 * hash + (this.trigger ? 1 : 0);
        hash = 83 * hash + this.alertCount;
        hash = 83 * hash + this.faultCount;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmsVO other = (AlarmsVO) obj;
        if (this.createdDate != other.createdDate) {
            return false;
        }
        if (Float.floatToIntBits(this.exceedPercent) != Float.floatToIntBits(other.exceedPercent)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.acknowledged != other.acknowledged) {
            return false;
        }
        if (this.deleted != other.deleted) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.trendValue) != Float.floatToIntBits(other.trendValue)) {
            return false;
        }
        if (this.createdYear != other.createdYear) {
            return false;
        }
        if (this.alarmCount != other.alarmCount) {
            return false;
        }
        if (this.trigger != other.trigger) {
            return false;
        }
        if (this.alertCount != other.alertCount) {
            return false;
        }
        if (this.faultCount != other.faultCount) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.alarmType, other.alarmType)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.sampleTimestamp, other.sampleTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.triggerTime, other.triggerTime)) {
            return false;
        }
        if (!Objects.equals(this.waveId, other.waveId)) {
            return false;
        }
        return Objects.equals(this.rowId, other.rowId);
    }

    @Override
    public String toString() {
        return "AlarmsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", paramName=" + paramName + ", alarmType=" + alarmType + ", apSetId=" + apSetId + ", createdDate=" + createdDate + ", exceedPercent=" + exceedPercent + ", highAlert=" + highAlert + ", highFault=" + highFault + ", acknowledged=" + acknowledged + ", deleted=" + deleted + ", lowAlert=" + lowAlert + ", lowFault=" + lowFault + ", paramType=" + paramType + ", sampleTimestamp=" + sampleTimestamp + ", sensorType=" + sensorType + ", trendValue=" + trendValue + ", triggerTime=" + triggerTime + ", waveId=" + waveId + ", rowId=" + rowId + ", createdYear=" + createdYear + ", alarmCount=" + alarmCount + ", trigger=" + trigger + ", alertCount=" + alertCount + ", faultCount=" + faultCount + '}';
    }

   

}
