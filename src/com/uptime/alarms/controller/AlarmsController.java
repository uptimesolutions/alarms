/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.uptime.alarms.delegate.CreateAlarmDelegate;
import com.uptime.alarms.delegate.DeleteAlarmDelegate;
import com.uptime.alarms.delegate.ReadAlarmDelegate;
import com.uptime.alarms.delegate.UpdateAlarmDelegate;
import com.uptime.alarms.util.JsonUtil;
import com.uptime.alarms.vo.AlarmsVO;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import com.uptime.cassandra.alarms.entity.AlarmCount;
import com.uptime.cassandra.alarms.entity.AlarmHistory;
import com.uptime.services.util.JsonConverterUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class AlarmsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AlarmsController.class.getName());
    ReadAlarmDelegate readDelegate;
    
    UpdateAlarmDelegate updateDelegate;

    /**
     * Constructor
     */
    public AlarmsController() {
       // CassandraConstants.SLF4J_CQL_LOG = LoggerFactory.getLogger(AlarmsController.class);
        readDelegate = new ReadAlarmDelegate();
        updateDelegate =new UpdateAlarmDelegate();
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteArea(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ActiveAlarms> result;
        StringBuilder json;

        if ((result = readDelegate.getAlarmsByCustomerSiteArea(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteAreaAsset(String customer, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ActiveAlarms> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmsByCustomerSiteAreaAsset(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }

        return json.toString();
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteAreaAssetPtLoc(String customer, String siteId, String areaId, String assetId, String pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ActiveAlarms> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmsByCustomerSiteAreaAssetPtLoc(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }

        return json.toString();
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteAreaAssetPtLocPoint(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ActiveAlarms> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmsByCustomerSiteAreaAssetPtLocPoint(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }

        return json.toString();
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param paramName, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteAreaAssetPtLocPointParam(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ActiveAlarms> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmsByCustomerSiteAreaAssetPtLocPointParam(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), paramName)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }

        return json.toString();
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByPK(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ActiveAlarms> result;
        StringBuilder json;

        if ((result = readDelegate.getAlarmsByPK(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), paramName, alarmType)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"alarmType\":\"").append(alarmType).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param createdYear, short
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteYearAreaAssetPtLoc(String customer, String siteId, short createdYear, String areaId, String assetId, String pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmHistory> result, allResult;
        allResult = new ArrayList<>();
        
        StringBuilder json;
        //Get the alarm_history data for the currrent year and the previous year for the given criterea
        if ((result = readDelegate.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer.trim().toUpperCase(), UUID.fromString(siteId), createdYear, UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId))) != null && !result.isEmpty()) {
            allResult.addAll(result);
        }
        if ((result = readDelegate.getAlarmsByCustomerSiteYearAreaAssetPtLoc(customer.trim().toUpperCase(), UUID.fromString(siteId), (short)(createdYear - 1), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId))) != null && !result.isEmpty()) {
            allResult.addAll(result);
        }
        
        if (!allResult.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"createdYear\":\"").append(createdYear).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < allResult.size(); i++) {
                json.append(JsonConverterUtil.toJSON(allResult.get(i))).append(i < allResult.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param createdYear, short
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(String customer, String siteId, short createdYear, String areaId, String assetId, String pointLocationId, String pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmHistory> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(customer.trim().toUpperCase(), UUID.fromString(siteId), createdYear, UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"createdYear\":\"").append(createdYear).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param createdYear, short
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param paramName, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(String customer, String siteId, short createdYear, String areaId, String assetId, String pointLocationId, String pointId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmHistory> result;
        StringBuilder json;

        if ((result = readDelegate.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(customer.trim().toUpperCase(), UUID.fromString(siteId), createdYear, UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), paramName)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"createdYear\":\"").append(createdYear).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param createdYear, short
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(String customer, String siteId, short createdYear, String areaId, String assetId, String pointLocationId, String pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmHistory> result;
        StringBuilder json;

        if ((result = readDelegate.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customer.trim().toUpperCase(), UUID.fromString(siteId), createdYear, UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), paramName, alarmType)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"createdYear\":\"").append(createdYear).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"alarmType\":\"").append(alarmType).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param createdYear, short
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @param rowId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(String customer, String siteId, short createdYear, String areaId, String assetId, String pointLocationId, String pointId, String paramName, String alarmType, String rowId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmHistory> result;
        StringBuilder json;

        if ((result = readDelegate.getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(customer.trim().toUpperCase(), UUID.fromString(siteId), createdYear, UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), paramName, alarmType, UUID.fromString(rowId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"createdYear\":\"").append(createdYear).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"alarmType\":\"").append(alarmType).append("\",")
                    .append("\"rowId\":\"").append(rowId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmCountByCustomerSiteAreaAssetPtLocPoint(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmCount> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmCountByCustomerSiteAreaAssetPtLocPoint(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmCountByCustomerSiteAreaAssetPtLoc(String customer, String siteId, String areaId, String assetId, String pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmCount> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmCountByCustomerSiteAreaAssetPtLoc(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmCountByCustomerSiteAreaAsset(String customer, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmCount> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmCountByCustomerSiteAreaAsset(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmCountByCustomerSiteArea(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmCount> result;
        StringBuilder json;
        
        if ((result = readDelegate.getAlarmCountByCustomerSiteArea(customer.trim().toUpperCase(), UUID.fromString(siteId), UUID.fromString(areaId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to
     * process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAlarmCountByCustomerSite(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        Map<String, List<ActiveAlarms>> activeAlarmsMap;
        List<AlarmCount> result;
        List<AlarmCount> alarmCountList = new ArrayList();
        List<ActiveAlarms> activeAlarmsList;
        int faltCount, alrtsCount;
        StringBuilder json;

        if ((result = readDelegate.getAlarmCountByCustomerSite(customer.trim().toUpperCase(), UUID.fromString(siteId))) != null && !result.isEmpty()) {
            activeAlarmsMap = new HashMap();

            for (AlarmCount alarmCount : result) {

                if (alarmCount.getFaultCount() + alarmCount.getAlertCount() != alarmCount.getAlarmCount()) {
                    if (activeAlarmsMap.containsKey(alarmCount.getAreaId().toString())) {
                        activeAlarmsList = activeAlarmsMap.get(alarmCount.getAreaId().toString());
                    } else {
                        activeAlarmsList = readDelegate.getAlarmsByCustomerSiteArea(alarmCount.getCustomerAccount(), alarmCount.getSiteId(), alarmCount.getAreaId());
                        activeAlarmsMap.put(alarmCount.getAreaId().toString(), activeAlarmsList);
                    }

                    activeAlarmsList = activeAlarmsList.stream().filter(al -> al.getAssetId().equals(alarmCount.getAssetId()) && al.getPointLocationId().equals(alarmCount.getPointLocationId()) && al.getPointId().equals(alarmCount.getPointId())).collect(Collectors.toList());

                    faltCount = 0;
                    alrtsCount = 0;

                    for (ActiveAlarms activeAlarms : activeAlarmsList) {
                        if (activeAlarms.getAlarmType().contains("FAULT")) {
                            faltCount = faltCount + 1;
                        } else if (activeAlarms.getAlarmType().contains("ALERT")) {
                            alrtsCount = alrtsCount + 1;
                        }
                    }
                    alarmCount.setAlertCount(alrtsCount);
                    alarmCount.setFaultCount(faltCount);
                    alarmCount.setAlarmCount(alrtsCount + faltCount);

                }
                alarmCountList.add(alarmCount);

            }

            if (!alarmCountList.isEmpty()) {
                updateDelegate.updateAlarmCount(alarmCountList);
            }

            json = new StringBuilder();
            json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"Alarms\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
        return json.toString();
    }

    /**
     * Create a new Alarm by inserting into Alarm table
     *
     * @param content, String Object
     * @return String Object
     */
    public String createAlarm(String content) {
        CreateAlarmDelegate createDelegate;
        AlarmsVO alarmsVO;

        if ((alarmsVO = JsonUtil.parser(content)) != null) {
            if (alarmsVO != null && alarmsVO.getCustomerAccount() != null && !alarmsVO.getCustomerAccount().isEmpty()
                    && alarmsVO.getSiteId() != null && alarmsVO.getAreaId() != null
                    && alarmsVO.getAssetId() != null && alarmsVO.getPointLocationId() != null
                    && alarmsVO.getPointId() != null
                    && alarmsVO.getParamName() != null && !alarmsVO.getParamName().isEmpty()
                    && alarmsVO.getAlarmType() != null && !alarmsVO.getAlarmType().isEmpty()) {
                
                // Insert into Cassandra
                try { 
                    createDelegate = new CreateAlarmDelegate();
                    return createDelegate.createAlarm(alarmsVO);
                } catch (IllegalArgumentException e) {
                    return "{\"outcome\":\"Error: Failed to create new Alarm.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Alarm.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given Alarm
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateAlarm(String content) {
        UpdateAlarmDelegate updateDelegate;
        AlarmsVO alarmsVO;
        List<AlarmsVO> alarmsList;
        JsonElement jsonElement;
        
        try {
            jsonElement = JsonParser.parseString(content);
            if (jsonElement.isJsonArray() && (alarmsList = JsonUtil.parserList(content)) != null) {
                if (alarmsList != null && !alarmsList.isEmpty()) {
                    for (AlarmsVO vo : alarmsList) {
                        if (!(vo != null && vo.getCustomerAccount() != null && !vo.getCustomerAccount().isEmpty()
                                && vo.getSiteId() != null && vo.getAreaId() != null
                                && vo.getAssetId() != null && vo.getPointLocationId() != null
                                && vo.getPointId() != null
                                && vo.getParamName() != null && !vo.getParamName().isEmpty()
                                && vo.getAlarmType() != null && !vo.getAlarmType().isEmpty())) {
                            return "{\"outcome\":\"insufficient data given in json\"}";
                        }
                    }
                    try {
                        updateDelegate = new UpdateAlarmDelegate();
                        return updateDelegate.updateAlarm(alarmsList);
                    } catch (IllegalArgumentException e) {
                        return "{\"outcome\":\"Error: Failed to delete Alarms.\"}";
                    }
                } else {
                    return "{\"outcome\":\"Insufficient data given in json\"}";
                }
            } else if (jsonElement.isJsonObject()) {
                if ((alarmsVO = JsonUtil.parser(content)) != null) {
                    if (alarmsVO != null && alarmsVO.getCustomerAccount() != null && !alarmsVO.getCustomerAccount().isEmpty()
                            && alarmsVO.getSiteId() != null && alarmsVO.getAreaId() != null
                            && alarmsVO.getAssetId() != null && alarmsVO.getPointLocationId() != null
                            && alarmsVO.getPointId() != null
                            && alarmsVO.getParamName() != null && !alarmsVO.getParamName().isEmpty()
                            && alarmsVO.getAlarmType() != null && !alarmsVO.getAlarmType().isEmpty()) {
                        
                        // Update in Cassandra
                        try {
                            updateDelegate = new UpdateAlarmDelegate();
                            return updateDelegate.updateAlarm(alarmsVO);
                        } catch (IllegalArgumentException e) {
                            return "{\"outcome\":\"Error: Failed to update Alarms.\"}";
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                            return "{\"outcome\":\"Error: Failed to update Alarms.\"}";
                        }
                        
                    } else {
                        return "{\"outcome\":\"insufficient data given in json\"}";
                    }
                } else {
                    return "{\"outcome\":\"Json is invalid\"}";
                }
            } else {
                return "{\"outcome\":\"Json is invalid\"}";
            }
        } catch (JsonSyntaxException e) {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given Alarm
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteAlarm(String content) {
        DeleteAlarmDelegate deleteDelegate;
        List<AlarmsVO> alarmsList;
        JsonElement jsonElement;
        
        try {
            if ((jsonElement = JsonParser.parseString(content)).isJsonObject()) {
                
                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteAlarmDelegate();
                    return deleteDelegate.deleteAlarm(JsonUtil.parser(content));
                } catch (IllegalArgumentException ex) {
                    return "{\"outcome\":\"Error: Failed to delete Alarms.\"}";
                }
            }
            
            if (jsonElement.isJsonArray() && (alarmsList = JsonUtil.parserList(content)) != null) {
                if (alarmsList != null && !alarmsList.isEmpty()) {
                    
                    // Delete from Cassandra
                    try { 
                        deleteDelegate = new DeleteAlarmDelegate();
                        return deleteDelegate.deleteAlarm(alarmsList);
                    } catch (IllegalArgumentException e) {
                        return "{\"outcome\":\"Error: Failed to delete Alarms.\"}";
                    }
                    
                } else {
                    return "{\"outcome\":\"Insufficient data given in json\"}";
                }
            } else {
                return "{\"outcome\":\"Json is invalid\"}";
            }
        } catch (JsonSyntaxException e) {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
