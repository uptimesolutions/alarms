/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.alarms.dao.AlarmMapperImpl;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import java.util.List;
import java.util.UUID;
import com.uptime.cassandra.alarms.dao.ActiveAlarmsDAO;
import com.uptime.cassandra.alarms.dao.AlarmCountDAO;
import com.uptime.cassandra.alarms.dao.AlarmHistoryDAO;
import com.uptime.cassandra.alarms.entity.AlarmCount;
import com.uptime.cassandra.alarms.entity.AlarmHistory;

/**
 *
 * @author madhavi
 */
public class ReadAlarmDelegate {
    private final ActiveAlarmsDAO activeAlarmsDAO;
    private final AlarmHistoryDAO alarmHistoryDAO;
    private final AlarmCountDAO alarmCountDAO;

    /**
     * Constructor
     */
    public ReadAlarmDelegate() {
        activeAlarmsDAO = AlarmMapperImpl.getInstance().activeAlarmsDAO();
        alarmHistoryDAO = AlarmMapperImpl.getInstance().alarmHistoryDAO();
        alarmCountDAO = AlarmMapperImpl.getInstance().alarmCountDAO();
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ActiveAlarms> getAlarmsByCustomerSiteArea(String customer, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return activeAlarmsDAO.findByCustomerSiteArea(customer, siteId, areaId);
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ActiveAlarms> getAlarmsByCustomerSiteAreaAsset(String customer, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return activeAlarmsDAO.findByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ActiveAlarms> getAlarmsByCustomerSiteAreaAssetPtLoc(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return activeAlarmsDAO.findByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId);
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ActiveAlarms> getAlarmsByCustomerSiteAreaAssetPtLocPoint(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return activeAlarmsDAO.findByCustomerSiteAreaAssetPtLocPoint(customer, siteId, areaId, assetId, pointLocationId,pointId);
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ActiveAlarms> getAlarmsByCustomerSiteAreaAssetPtLocPointParam(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return activeAlarmsDAO.findByCustomerSiteAreaAssetPtLocPointParam(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName);
    }

    /**
     * Return a List of ActiveAlarms Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ActiveAlarms> getAlarmsByPK(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return activeAlarmsDAO.findByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType);
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmHistory> getAlarmsByCustomerSiteYearAreaAssetPtLoc(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmHistoryDAO.findByCustomerSiteYearAreaAssetPtLoc(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId);
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmHistory> getAlarmsByCustomerSiteYearAreaAssetPtLocPoint(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmHistoryDAO.findByCustomerSiteYearAreaAssetPtLocPoint(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId);
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmHistory> getAlarmsByCustomerSiteYearAreaAssetPtLocPointParam(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmHistoryDAO.findByCustomerSiteYearAreaAssetPtLocPointParam(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName);
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmHistory> getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmHistoryDAO.findByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType);
    }

    /**
     * Return a List of AlarmHistory Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @param rowId, UUID Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmHistory> getAlarmsByCustomerSiteYearAreaAssetPtLocPointParamAlarmTypeRow(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType, UUID rowId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmHistoryDAO.findByPK(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, rowId);
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmCount> getAlarmCountByCustomerSiteArea(String customer, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmCountDAO.findByCustomerSiteArea(customer, siteId, areaId);
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmCount> getAlarmCountByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmCountDAO.findByCustomerSite(customer, siteId);
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmCount> getAlarmCountByCustomerSiteAreaAsset(String customer, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmCountDAO.findByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmCount> getAlarmCountByCustomerSiteAreaAssetPtLoc(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmCountDAO.findByCustomerSiteAreaAssetPtLoc(customer, siteId, areaId, assetId, pointLocationId);
    }

    /**
     * Return a List of AlarmCount Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's not enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmCount> getAlarmCountByCustomerSiteAreaAssetPtLocPoint(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmCountDAO.findByPK(customer, siteId, areaId, assetId, pointLocationId, pointId);
    }

}
