/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.delegate;

import static com.uptime.alarms.AlarmsService.sendEvent;
import static com.uptime.alarms.AlarmsService.sendKafkaMessage;
import com.uptime.alarms.util.DelegateUtil;
import com.uptime.alarms.vo.AlarmsVO;
import com.uptime.cassandra.alarms.dao.AlarmMapperImpl;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import com.uptime.cassandra.alarms.entity.AlarmCount;
import java.util.List;
import com.uptime.cassandra.alarms.dao.ActiveAlarmsDAO;
import com.uptime.cassandra.alarms.dao.AlarmCountDAO;
import com.uptime.cassandra.alarms.dao.AlarmHistoryDAO;
import com.uptime.cassandra.alarms.entity.AlarmHistory;
import com.uptime.services.util.ServiceUtil;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateAlarmDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAlarmDelegate.class.getName());
    private final ActiveAlarmsDAO activeAlarmsDAO;
    private final AlarmHistoryDAO alarmHistoryDAO;
    private final AlarmCountDAO alarmCountDAO;

    /**
     * Constructor
     */
    public UpdateAlarmDelegate() {
        activeAlarmsDAO = AlarmMapperImpl.getInstance().activeAlarmsDAO();
        alarmHistoryDAO = AlarmMapperImpl.getInstance().alarmHistoryDAO();
        alarmCountDAO = AlarmMapperImpl.getInstance().alarmCountDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param alarmsVO, AlarmsVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateAlarm(AlarmsVO alarmsVO) throws IllegalArgumentException, Exception {
        List<ActiveAlarms> alarmsList;
        ActiveAlarms newAlarms;
        ActiveAlarms originalAlarms = null;
        AlarmHistory alarmHistory;
        
        List<AlarmCount> alarmCountList;
        AlarmCount newAlarmCount;
        AlarmCount originalAlarmCount = null;

        String errorMsg;
        
        // Find the entities based on the given values
        try {
            if ((alarmsList = activeAlarmsDAO.findByPK(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId(), alarmsVO.getParamName(), alarmsVO.getAlarmType())) != null && !alarmsList.isEmpty()) {
                originalAlarms = alarmsList.get(0);
            }
            if ((alarmCountList = alarmCountDAO.findByPK(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId())) != null && !alarmCountList.isEmpty()) {
                originalAlarmCount = alarmCountList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }

        if (originalAlarms != null && originalAlarmCount != null) {
            newAlarms = DelegateUtil.getActiveAlarms(alarmsVO);
            newAlarmCount = DelegateUtil.getAlarmCount(alarmsVO);
            newAlarmCount.setAlarmCount(originalAlarmCount.getAlarmCount());
            newAlarmCount.setAlertCount(originalAlarmCount.getAlertCount());
            newAlarmCount.setFaultCount(originalAlarmCount.getFaultCount());
            alarmHistory = DelegateUtil.getAlarmHistory(alarmsVO);
            if (alarmHistory.getRowId() == null) {
                alarmHistory.setRowId(UUID.randomUUID());
            }

            if (alarmHistory.getCreatedYear() == 0) {
                ZonedDateTime zdt = originalAlarms.getTriggerTime().atZone(ZoneOffset.UTC);
                alarmHistory.setCreatedYear((short) zdt.getYear());
            }
            
            if (!originalAlarms.isAcknowledged() && newAlarms.isAcknowledged()) {
                //Insert updated entities into Cassandra if original entities are found
                //update the alarm_counts table and delete from active_alarms table
                newAlarmCount.setAlarmCount(Math.max(0, newAlarmCount.getAlarmCount() - 1));
                newAlarmCount.setAlertCount(Math.max(0, newAlarmCount.getAlertCount() - 1));
                newAlarmCount.setFaultCount(Math.max(0, newAlarmCount.getFaultCount() - 1));
                try { 
                    if (((errorMsg = ServiceUtil.validateObjectData(originalAlarms)) == null) &&
                            ((errorMsg = ServiceUtil.validateObjectData(newAlarmCount)) == null) &&
                            ((errorMsg = ServiceUtil.validateObjectData(alarmHistory)) == null)) {
                        activeAlarmsDAO.delete(originalAlarms);
                        alarmCountDAO.create(newAlarmCount);
                        alarmHistoryDAO.create(alarmHistory);
                        
                        //Send message to KafkaProxy
                        sendKafkaMessage(alarmsVO);
                        return "{\"outcome\":\"Updated Alarms successfully.\"}";
                    } else {
                        return errorMsg;
                    }
                } catch ( IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    sendEvent(e.getStackTrace());
                }
                return "{\"outcome\":\"Error: Failed to update Alarms.\"}";
            } else {
                return "{\"outcome\":\"No changes found to update Alarms.\"}";
            }
        }

        return "{\"outcome\":\"Alarms not found to update.\"}";
    }

    /**
     * Update Rows in Cassandra based on the given alarms objects
     *
     * @param alarms, ActiveAlarms list
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateAlarm(List<AlarmsVO> alarms) throws IllegalArgumentException {
        try {
            for (AlarmsVO alarm : alarms) {
                updateAlarm(alarm);
            }
            return "{\"outcome\":\"Updated Alarms successfully.\"}";
        } catch ( IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Alarms.\"}";
    }

    
    
    
    /**
     * Update Rows in Cassandra based on the given alarmCount objects
     *
     * @param alarmCountList
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateAlarmCount(List<AlarmCount> alarmCountList) throws IllegalArgumentException {
        try {

            alarmCountDAO.update(alarmCountList);

            return "{\"outcome\":\"Updated Alarms Count List successfully.\"}";
            
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Alarms Count List .\"}";
    }

}
