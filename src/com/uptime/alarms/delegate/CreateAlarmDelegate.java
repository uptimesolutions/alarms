/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.delegate;

import static com.uptime.alarms.AlarmsService.sendEvent;
import static com.uptime.alarms.AlarmsService.sendKafkaMessage;
import com.uptime.alarms.util.DelegateUtil;
import com.uptime.cassandra.alarms.dao.AlarmMapperImpl;
import com.uptime.alarms.vo.AlarmsVO;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import com.uptime.cassandra.alarms.dao.ActiveAlarmsDAO;
import com.uptime.cassandra.alarms.dao.AlarmCountDAO;
import com.uptime.cassandra.alarms.entity.AlarmCount;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateAlarmDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAlarmDelegate.class.getName());
    private final PointsDAO pointsDAO;
    private final ActiveAlarmsDAO activeAlarmsDAO;
    private final AlarmCountDAO alarmCountDAO;

    /**
     * Constructor
     */
    public CreateAlarmDelegate() {
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        activeAlarmsDAO = AlarmMapperImpl.getInstance().activeAlarmsDAO();
        alarmCountDAO = AlarmMapperImpl.getInstance().alarmCountDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param alarmsVO, AlarmsVO object
     * @return String object
     * @throws Exception
     * @throws IllegalArgumentException
     */
    public String createAlarm(AlarmsVO alarmsVO) throws IllegalArgumentException, Exception {
        ActiveAlarms alarms;
        AlarmCount alarmCount;
        
        List<Points> pointsList;
        Points point = null;
        
        List<ActiveAlarms> alarmsList;
        ActiveAlarms originalAlarms = null;
        
        List<AlarmCount> alarmCountList;
        AlarmCount originalAlarmCount = null;
        String errorMsg;
        
        if (alarmsVO != null) {
            try {
                if ((pointsList = pointsDAO.findByCustomerSiteAreaAssetLocationPoint(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId())) != null && !pointsList.isEmpty()) {
                    point = pointsList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find point.\"}";
            }

            if (point != null && point.isAlarmEnabled()) {
                if ((alarmsList = activeAlarmsDAO.findByPK(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId(), alarmsVO.getParamName(), alarmsVO.getAlarmType())) != null && !alarmsList.isEmpty()) {
                    originalAlarms = alarmsList.get(0);
                }

                if ((alarmCountList = alarmCountDAO.findByPK(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId())) != null && !alarmCountList.isEmpty()) {
                    originalAlarmCount = alarmCountList.get(0);
                }

                if (originalAlarms == null || originalAlarms.isAcknowledged()) {

                    alarms = DelegateUtil.getActiveAlarms(alarmsVO);
                    alarmCount = DelegateUtil.getAlarmCount(alarmsVO);

                    if (originalAlarmCount != null) {
                        alarmCount.setAlarmCount(originalAlarmCount.getAlarmCount() + 1);
                        if (alarms.getAlarmType().contains("ALERT")) {
                            alarmCount.setAlertCount(originalAlarmCount.getAlertCount() + 1);
                        } else if (alarms.getAlarmType().contains("FAULT")) {
                            alarmCount.setFaultCount(originalAlarmCount.getFaultCount() + 1);
                        }
                    } else {
                        alarmCount.setAlarmCount(1);
                        if (alarms.getAlarmType().contains("FAULT")) {
                            alarmCount.setFaultCount(1);
                        } else if (alarms.getAlarmType().contains("ALERT")) {
                            alarmCount.setAlertCount(1);
                        }
                    }
                    
                    // Insert the entities into Cassandra.
                    try { 
                        if (((errorMsg = ServiceUtil.validateObjectData(alarms)) == null) &&
                                ((errorMsg = ServiceUtil.validateObjectData(alarmCount)) == null)) {
                            activeAlarmsDAO.create(alarms);
                            alarmCountDAO.create(alarmCount);

                            //Send message to Kafka
                            sendKafkaMessage(alarmsVO);

                            return "{\"outcome\":\"New Alarm created successfully.\"}";
                        } else {
                            return errorMsg;
                        }
                    } catch ( IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                    }
                }
            }

            if (point != null && !point.isAlarmEnabled()) {
                return "{\"outcome\":\"Error: Alarm not created as the alarm is not enabled on the Point.\"}";
            } else {
                if (originalAlarms != null && !originalAlarms.isAcknowledged()) {
                    sendKafkaMessage(alarmsVO);
                    return "{\"outcome\":\"Alarm cannot be created since an unacknowledged alarms already exists.\"}";
                }
                else 
                    return "{\"outcome\":\"Error: Failed to find point.\"}";
            }
        }
        return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
    }

}
