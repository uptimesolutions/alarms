/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.delegate;

import static com.uptime.alarms.AlarmsService.sendEvent;
import com.uptime.alarms.util.DelegateUtil;
import com.uptime.alarms.vo.AlarmsVO;
import com.uptime.cassandra.alarms.dao.AlarmMapperImpl;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import java.util.List;
import com.uptime.cassandra.alarms.dao.ActiveAlarmsDAO;
import com.uptime.cassandra.alarms.dao.AlarmCountDAO;
import com.uptime.cassandra.alarms.entity.AlarmCount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteAlarmDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAlarmDelegate.class.getName());
    private final ActiveAlarmsDAO activeAlarmsDAO;
    private final AlarmCountDAO alarmCountDAO;

    /**
     * Constructor
     */
    public DeleteAlarmDelegate() {
        activeAlarmsDAO = AlarmMapperImpl.getInstance().activeAlarmsDAO();
        alarmCountDAO = AlarmMapperImpl.getInstance().alarmCountDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param alarmsVO, AlarmsVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteAlarm(AlarmsVO alarmsVO) throws IllegalArgumentException {
        List<ActiveAlarms> alarmsList;
        ActiveAlarms alarms = null;

        List<AlarmCount> alarmCountList;
        AlarmCount newAlarmCount = null;
        AlarmCount originalAlarmCount = null;
        
        // Find the entities based on the given values
        if (alarmsVO != null) {
            try {
                if ((alarmsList = activeAlarmsDAO.findByPK(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId(), alarmsVO.getParamName(), alarmsVO.getAlarmType())) != null && !alarmsList.isEmpty()) {
                    alarms = alarmsList.get(0);
                }
                if ((alarmCountList = alarmCountDAO.findByPK(alarmsVO.getCustomerAccount(), alarmsVO.getSiteId(), alarmsVO.getAreaId(), alarmsVO.getAssetId(), alarmsVO.getPointLocationId(), alarmsVO.getPointId())) != null && !alarmCountList.isEmpty()) {
                    originalAlarmCount = alarmCountList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find Alarms to delete.\"}";
            }

            if (originalAlarmCount != null && alarms != null && !alarms.isAcknowledged()) {
                newAlarmCount = DelegateUtil.getAlarmCount(alarmsVO);
                newAlarmCount.setAlarmCount(Math.max(0, originalAlarmCount.getAlarmCount() - 1));
                newAlarmCount.setAlertCount(Math.max(0, originalAlarmCount.getAlertCount() - 1));
                newAlarmCount.setFaultCount(Math.max(0, originalAlarmCount.getFaultCount() - 1));
            }

            // Delete the entities from Cassandra
            try {
                if (alarms != null) {
                    activeAlarmsDAO.delete(alarms);
                    if (newAlarmCount != null) {
                        alarmCountDAO.update(newAlarmCount);
                    }
                    return "{\"outcome\":\"Deleted Alarms successfully.\"}";
                } else {
                    return "{\"outcome\":\"No Alarms found to delete.\"}";
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to delete Alarms.\"}";
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param alarmsVOs, AlarmsVO list
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteAlarm(List<AlarmsVO> alarmsVOs) throws IllegalArgumentException {
        try {
            alarmsVOs.forEach(alarm -> deleteAlarm(alarm));
            return "{\"outcome\":\"Deleted Alarms successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Alarms.\"}";
    }

}
