/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.util;

import com.uptime.alarms.vo.AlarmsVO;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import com.uptime.cassandra.alarms.entity.AlarmCount;
import com.uptime.cassandra.alarms.entity.AlarmHistory;

/**
 *
 * @author madhavi
 */
public class DelegateUtil {
    /**
     * Convert AlarmsVO Object to ActiveAlarms Object
     *
     * @param alarmsVO, AlarmsVO Object
     * @return ActiveAlarms Object
     */
    public static ActiveAlarms getActiveAlarms(AlarmsVO alarmsVO) {
        ActiveAlarms entity = new ActiveAlarms();
        if (alarmsVO != null) {
            entity.setCustomerAccount(alarmsVO.getCustomerAccount());
            entity.setSiteId(alarmsVO.getSiteId());
            entity.setAreaId(alarmsVO.getAreaId());
            entity.setAssetId(alarmsVO.getAssetId());
            entity.setPointLocationId(alarmsVO.getPointLocationId());
            entity.setPointId(alarmsVO.getPointId());
            entity.setParamName(alarmsVO.getParamName());
            entity.setAlarmType(alarmsVO.getAlarmType());
            entity.setApSetId(alarmsVO.getApSetId());
            entity.setAcknowledged(alarmsVO.isAcknowledged());
            entity.setDeleted(alarmsVO.isDeleted());
            entity.setExceedPercent(alarmsVO.getExceedPercent());
            entity.setHighAlert(alarmsVO.getHighAlert());
            entity.setHighFault(alarmsVO.getHighFault());
            entity.setLowAlert(alarmsVO.getLowAlert());
            entity.setLowFault(alarmsVO.getLowFault());
            entity.setParamType(alarmsVO.getParamType());
            entity.setSampleTimestamp(alarmsVO.getSampleTimestamp());
            entity.setSensorType(alarmsVO.getSensorType());
            entity.setTrendValue(alarmsVO.getTrendValue());
            entity.setTriggerTime(alarmsVO.getTriggerTime());
        }
        return entity;
    }

    /**
     * Convert AlarmsVO Object to AlarmHistory Object
     *
     * @param alarmsVO, AlarmsVO Object
     * @return AlarmHistory Object
     */
    public static AlarmHistory getAlarmHistory(AlarmsVO alarmsVO) {
        AlarmHistory entity = new AlarmHistory();
        if (alarmsVO != null) {
            entity.setCustomerAccount(alarmsVO.getCustomerAccount());
            entity.setSiteId(alarmsVO.getSiteId());
            entity.setAreaId(alarmsVO.getAreaId());
            entity.setAssetId(alarmsVO.getAssetId());
            entity.setPointLocationId(alarmsVO.getPointLocationId());
            entity.setPointId(alarmsVO.getPointId());
            entity.setParamName(alarmsVO.getParamName());
            entity.setAlarmType(alarmsVO.getAlarmType());
            entity.setRowId(alarmsVO.getRowId());
            entity.setAcknowledged(alarmsVO.isAcknowledged());
            entity.setCreatedYear(alarmsVO.getCreatedYear());
            entity.setExceedPercent(alarmsVO.getExceedPercent());
            entity.setHighAlert(alarmsVO.getHighAlert());
            entity.setHighFault(alarmsVO.getHighFault());
            entity.setLowAlert(alarmsVO.getLowAlert());
            entity.setLowFault(alarmsVO.getLowFault());
            entity.setParamType(alarmsVO.getParamType());
            entity.setSampleTimestamp(alarmsVO.getSampleTimestamp());
            entity.setSensorType(alarmsVO.getSensorType());
            entity.setTrendValue(alarmsVO.getTrendValue());
            entity.setTriggerTime(alarmsVO.getTriggerTime());
        }
        return entity;
    }

    /**
     * Convert AlarmsVO Object to AlarmCount Object
     *
     * @param alarmsVO, AlarmsVO Object
     * @return AlarmCount Object
     */
    public static AlarmCount getAlarmCount(AlarmsVO alarmsVO) {
        AlarmCount entity = new AlarmCount();
        if (alarmsVO != null) {
            entity.setCustomerAccount(alarmsVO.getCustomerAccount());
            entity.setSiteId(alarmsVO.getSiteId());
            entity.setAreaId(alarmsVO.getAreaId());
            entity.setAssetId(alarmsVO.getAssetId());
            entity.setPointLocationId(alarmsVO.getPointLocationId());
            entity.setPointId(alarmsVO.getPointId());
            entity.setAlarmCount(alarmsVO.getAlarmCount());
            entity.setAlertCount(alarmsVO.getAlertCount());
            entity.setFaultCount(alarmsVO.getFaultCount());
        }
        return entity;
    }

}
