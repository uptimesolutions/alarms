/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.alarms.vo.AlarmsVO;
import static com.uptime.services.util.HttpUtils.parseInstant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class JsonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class.getName());

    /**
     * Parse the given json String Object and return a AlarmsVO object
     *
     * @param content, String Object
     * @return AlarmsVO Object
     */
    public static AlarmsVO parser(String content) {
        AlarmsVO alarmsVO = null;
        JsonElement element;
        JsonObject jsonObject;
        LOGGER.info("Json - {}", content);

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            alarmsVO = new AlarmsVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    alarmsVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.info("Exception in customerAccount - {}", ex.toString());
                }
            }
            if (jsonObject.has("siteId")) {
                try {
                    alarmsVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in siteId - {}", ex.toString());
                }
            }
            if (jsonObject.has("createdYear")) {
                try {
                    alarmsVO.setCreatedYear(jsonObject.get("createdYear").getAsShort());
                } catch (Exception ex) {
                    LOGGER.info("Exception in createdYear - {}", ex.toString());
                }
            }
            if (jsonObject.has("areaId")) {
                try {
                    alarmsVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in areaId - {}", ex.toString());
                }
            }
            if (jsonObject.has("assetId")) {
                try {
                    alarmsVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in assetId - {}", ex.toString());
                }
            }
            if (jsonObject.has("pointLocationId")) {
                try {
                    alarmsVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in pointLocationId - {}", ex.toString());
                }
            }
            if (jsonObject.has("pointId")) {
                try {
                    alarmsVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in pointId - {}", ex.toString());
                }
            }
            if (jsonObject.has("apSetId")) {
                try {
                    alarmsVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in apSetId - {}", ex.toString());
                }
            }
            if (jsonObject.has("paramName")) {
                try {
                    alarmsVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.info("Exception in paramName - {}", ex.toString());
                }
            }
            if (jsonObject.has("alarmType")) {
                try {
                    alarmsVO.setAlarmType(jsonObject.get("alarmType").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.info("Exception in alarmType - {}", ex.toString());
                }
            }
            if (jsonObject.has("exceedPercent")) {
                try {
                    alarmsVO.setExceedPercent(jsonObject.get("exceedPercent").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.info("Exception in exceedPercent - {}", ex.toString());
                }
            }
            if (jsonObject.has("highAlert")) {
                try {
                    alarmsVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.info("Exception in highAlert - {}", ex.toString());
                }
            }
            if (jsonObject.has("highFault")) {
                try {
                    alarmsVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.info("Exception in highFault - {}", ex.toString());
                }
            }
            if (jsonObject.has("lowAlert")) {
                try {
                    alarmsVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.info("Exception in lowAlert - {}", ex.toString());
                }
            }
            if (jsonObject.has("lowFault")) {
                try {
                    alarmsVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.info("Exception in lowFault - {}", ex.toString());
                }
            }
            if (jsonObject.has("acknowledged")) {
                try {
                    alarmsVO.setAcknowledged(jsonObject.get("acknowledged").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.info("Exception in acknowledged - {}", ex.toString());
                }
            }
            if (jsonObject.has("deleted")) {
                try {
                    alarmsVO.setDeleted(jsonObject.get("deleted").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.info("Exception in deleted - {}", ex.toString());
                }
            }
            if (jsonObject.has("paramType")) {
                try {
                    alarmsVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.info("Exception in paramType - {}", ex.toString());
                }
            }
            if (jsonObject.has("sampleTimestamp")) {
                try {
                    alarmsVO.setSampleTimestamp(parseInstant(jsonObject.get("sampleTimestamp").getAsString()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in LocalDateTime sampleTimestamp - {}", ex.toString());
                }
            }
            if (jsonObject.has("sensorType")) {
                try {
                    alarmsVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.info("Exception in sensorType - {}", ex.toString());
                }
            }

            if (jsonObject.has("trendValue")) {
                try {
                    alarmsVO.setTrendValue(jsonObject.get("trendValue").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.info("Exception in trendValue - {}", ex.toString());
                }
            }
            if (jsonObject.has("triggerTime")) {
                try {
                    alarmsVO.setTriggerTime(parseInstant(jsonObject.get("triggerTime").getAsString()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in triggerTime - {}", ex.toString());
                }
            }
            if (jsonObject.has("waveId")) {
                try {
                    alarmsVO.setWaveId(UUID.fromString(jsonObject.get("waveId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in waveId - {}", ex.toString());
                }
            }
            if (jsonObject.has("rowId")) {
                try {
                    alarmsVO.setRowId(UUID.fromString(jsonObject.get("rowId").getAsString().trim()));
                } catch (Exception ex) {
                    LOGGER.info("Exception in rowId - {}", ex.toString());
                }
            }
            if (jsonObject.has("alarmCount")) {
                try {
                    alarmsVO.setAlarmCount(jsonObject.get("alarmCount").getAsInt());
                } catch (Exception ex) {
                    LOGGER.info("Exception in alarmCount - {}", ex.toString());
                }
            }
        }

        return alarmsVO;
    }

    /**
     * Parse the given json String Object and return a AlarmsVOs object
     *
     * @param content, String Object
     * @return AlarmsVO list
     */
    public static List<AlarmsVO> parserList(String content) {
        List<AlarmsVO> alarmsList = new ArrayList<>();
        JsonArray jsonArray;
        JsonElement element;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null) {
            if ((jsonArray = element.getAsJsonArray()) != null) {
                jsonArray.forEach(element1 -> {
                    JsonObject jsonObject = element1.getAsJsonObject();
                    AlarmsVO alarmsVO = new AlarmsVO();
                    if (jsonObject.has("customerAccount")) {
                        try {
                            alarmsVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in customerAccount - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("siteId")) {
                        try {
                            alarmsVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in siteId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("createdYear")) {
                        try {
                            alarmsVO.setCreatedYear(jsonObject.get("createdYear").getAsShort());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in createdYear - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("areaId")) {
                        try {
                            alarmsVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in areaId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("assetId")) {
                        try {
                            alarmsVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in assetId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("pointLocationId")) {
                        try {
                            alarmsVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in pointLocationId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("pointId")) {
                        try {
                            alarmsVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in pointId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("apSetId")) {
                        try {
                            alarmsVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in apSetId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("paramName")) {
                        try {
                            alarmsVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in paramName - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("alarmType")) {
                        try {
                            alarmsVO.setAlarmType(jsonObject.get("alarmType").getAsString().trim());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in alarmType - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("exceedPercent")) {
                        try {
                            alarmsVO.setExceedPercent(jsonObject.get("exceedPercent").getAsFloat());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in exceedPercent - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("highAlert")) {
                        try {
                            alarmsVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in highAlert - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("highFault")) {
                        try {
                            alarmsVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in highFault - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("lowAlert")) {
                        try {
                            alarmsVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in lowAlert - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("lowFault")) {
                        try {
                            alarmsVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in lowFault - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("acknowledged")) {
                        try {
                            alarmsVO.setAcknowledged(jsonObject.get("acknowledged").getAsBoolean());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in acknowledged - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("deleted")) {
                        try {
                            alarmsVO.setDeleted(jsonObject.get("deleted").getAsBoolean());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in deleted - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("paramType")) {
                        try {
                            alarmsVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in paramType - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("sampleTimestamp")) {
                        try {
                            alarmsVO.setSampleTimestamp(parseInstant(jsonObject.get("sampleTimestamp").getAsString()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in sampleTimestamp - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("sensorType")) {
                        try {
                            alarmsVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in sensorType - {}", ex.toString());
                        }
                    }

                    if (jsonObject.has("trendValue")) {
                        try {
                            alarmsVO.setTrendValue(jsonObject.get("trendValue").getAsFloat());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in trendValue - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("triggerTime")) {
                        try {
                            alarmsVO.setTriggerTime(parseInstant(jsonObject.get("triggerTime").getAsString()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in triggerTime - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("waveId")) {
                        try {
                            alarmsVO.setWaveId(UUID.fromString(jsonObject.get("waveId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in waveId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("rowId")) {
                        try {
                            alarmsVO.setWaveId(UUID.fromString(jsonObject.get("rowId").getAsString().trim()));
                        } catch (Exception ex) {
                            LOGGER.info("Exception in rowId - {}", ex.toString());
                        }
                    }
                    if (jsonObject.has("alarmCount")) {
                        try {
                            alarmsVO.setAlarmCount(jsonObject.get("alarmCount").getAsInt());
                        } catch (Exception ex) {
                            LOGGER.info("Exception in alarmCount - {}", ex.toString());
                        }
                    }
                    alarmsList.add(alarmsVO);

                });
            }
        }
        return alarmsList;
    }
}
