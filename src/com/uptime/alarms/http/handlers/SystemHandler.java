/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.http.handlers;

import static com.uptime.alarms.AlarmsService.IP_ADDRESS;
import static com.uptime.alarms.AlarmsService.PORT;
import static com.uptime.alarms.AlarmsService.SERVICE_NAME;
import static com.uptime.alarms.AlarmsService.mutex;
import static com.uptime.alarms.AlarmsService.names;
import static com.uptime.alarms.AlarmsService.running;
import com.uptime.services.http.handler.AbstractSystemHandlerVersion2;
import java.util.concurrent.Semaphore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
public class SystemHandler extends AbstractSystemHandlerVersion2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemHandler.class.getName());

    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public void setRunning(boolean value) {
        running = value;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    @Override
    public int getServicePort() {
        return PORT;
    }

    @Override
    public String getServiceIpAddress() {
        return IP_ADDRESS;
    }

    @Override
    public String[] getServiceSubscribeNames() {
        return names;
    }

    @Override
    public Semaphore getMutex() {
        return mutex;
    }

    @Override
    public void sendEvent(StackTraceElement[] stackTrace) {
        com.uptime.alarms.AlarmsService.sendEvent(stackTrace);
    }
}
