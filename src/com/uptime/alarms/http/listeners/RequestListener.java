/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.alarms.http.listeners;

import com.sun.net.httpserver.HttpServer;
import com.uptime.alarms.http.handlers.AlarmsHandler;
import com.uptime.alarms.http.handlers.SystemHandler;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
public class RequestListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestListener.class.getName());
    private int port = 0;
    HttpServer server;
  
    public RequestListener(int port){
        this.port = port;
    }
    
    public void stop(){
        LOGGER.info("Stopping request listener...");
        server.stop(10);
    }
    
    public void start() throws IOException{
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/alarms", new AlarmsHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.info("Starting request listener...");
        server.start();

    }
}
